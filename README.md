
<!-- README.md is generated from README.Rmd. Please edit that file -->

# Required sample tool

[![Project Status: Inactive – The project has reached a stable, usable
state but is no longer being actively developed; support/maintenance
will be provided as time
allows.](https://www.repostatus.org/badges/latest/inactive.svg)](https://www.repostatus.org/#inactive)
[![](https://img.shields.io/badge/app%20status-online-brightgreen.svg)](https://requiredsample.geohealthresearch.org/)

### Development

*Vincent Herbreteau*, UMR Espace-Dev, Institut de Recherche pour le
Développement (IRD), Cambodia
